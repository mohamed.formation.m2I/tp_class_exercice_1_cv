// local reviews data
import Person from "./Person.js"

// const reviews = [
//   {
//     id: 1,
//     name: "susan smith",
//     job: "web developer",
//     img:
//       "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg",
//     text:
//       "Développeur web diplômé d’une LP métiers de l’informatique ayant travaillé plus d’un an en tant qu’alternant puis employé d’une grande entreprise de sous-traitance de programmation web. Je souhaite évoluer dans votre entreprise et travailler sur vos projets dans le domaine de la course automobile car c’est ce qui me passionne.",
//   },
//   {
//     id: 2,
//     name: "anna johnson",
//     job: "web designer",
//     img:
//       "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg",
//     text:
//       "Développeur full stack certifié Scrum, avec plus de 2 ans d’expérience. Mordu d’informatique, j’ai appris à coder dès mon plus jeune âge dans divers langages informatiques (Javascript, PHP, AngularJS…) et ai créé des projets personnels, dont un site référençant les vidéos les plus vues sur la plateforme Twitch (+ 5000 visites/mois).",
//   },
//   {
//     id: 3,
//     name: "Abdallah Jones",
//     job: "Web Developper",
//     img:
//       "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg",
//     text:
//       `Hello, je m'appelle Abdallah. J'aime le sport, les series et les animaux. Je suis développeur depuis plus de 10 ans.
//        J'apprécie particulièrement le javascript et le Java. Je cherche actuellement une nouvelle opportunité pro`,
//   },
//   {
//     id: 4,
//     name: "bill anderson",
//     job: "the boss",
//     img:
//       "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg",
//     text:
//       "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. ",
//   },
// ];


let person1 = new Person();
let person2 = new Person();
let person3 = new Person();
let person4 = new Person();

person1.id = 1;
person1.name = "susan smith";
person1.job = "web developer";
person1.img = "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg";
person1.text = "Développeur web diplômé d’une LP métiers de l’informatique ayant travaillé plus d’un an en tant qu’alternant puis employé d’une grande entreprise de sous-traitance de programmation web. Je souhaite évoluer dans votre entreprise et travailler sur vos projets dans le domaine de la course automobile car c’est ce qui me passionne.";

person2.id = 2;
person2.name = "anna johnson";
person2.job = "web developer";
person2.img = "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg";
person2.text = "Développeur full stack certifié Scrum, avec plus de 2 ans d’expérience. Mordu d’informatique, j’ai appris à coder dès mon plus jeune âge dans divers langages informatiques (Javascript, PHP, AngularJS…) et ai créé des projets personnels, dont un site référençant les vidéos les plus vues sur la plateforme Twitch (+ 5000 visites/mois).";

person3.id = 3;
person3.name = "Abdallah Jones";
person3.job = "web developer";
person3.img = "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg";
person3.text = "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. ";

person4.id = 4;
person4.name = "bill anderson";
person4.job = "The boss";
person4.img = "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg";
person4.text = "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. ";

const reviews = [person1, person2, person3, person4];


// select items
const img = document.getElementById("person-img");
const author = document.getElementById("author");
const job = document.getElementById("job");
const info = document.getElementById("info");

const prevBtn = document.querySelector(".prev-btn");
const nextBtn = document.querySelector(".next-btn");
const randomBtn = document.querySelector(".random-btn");

// set starting item
let currentItem = 0;

// load initial item
window.addEventListener("DOMContentLoaded", function () {
  const item = reviews[currentItem];
  alert(item);
  img.src = item.img;
  author.textContent = item.name;
  job.textContent = item.job;
  info.textContent = item.text;
});

// show person based on item
function showPerson(person) {
  const item = reviews[person];
  img.src = item.img;
  author.textContent = item.name;
  job.textContent = item.job;
  info.textContent = item.text;
}
// show next person
nextBtn.addEventListener("click", function () {
  currentItem++;
  if (currentItem > reviews.length - 1) {
    currentItem = 0;
  }
  showPerson(currentItem);
});
// show prev person
prevBtn.addEventListener("click", function () {
  currentItem--;
  if (currentItem < 0) {
    currentItem = reviews.length - 1;
  }
  showPerson(currentItem);
});
// show random person
randomBtn.addEventListener("click", function () {
  console.log("hello");

  currentItem = Math.floor(Math.random() * reviews.length);
  showPerson(currentItem);
});
